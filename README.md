## Introduction

This `adas-data-platform` is a in-house developed tool that provides multiple support for ADAS road test and engineering data replay

## Compilation requirements
* [Build and compile firmware-adas](https://bitbucket.org/sfmotors/firmware-adas/src/master/ADAS/)

## Setup Python environment
* [Python](https://docs.python.org/3/) >= 3.5.2

* Open your terminal and enter the command, init python enviroment path
       `export PYTHONPATH=/home/[user]/catkin_ws/devel/lib/python2.7/dist-packages:/opt/ros/kinetic/lib/python2.7/dist-packages`
* Install pip3
       `sudo apt-get install python3-pip`
* Install websockets
       `sudo pip3 install websockets` 
* Install yaml
       `sudo pip3 install pyyaml`
* Install rospkg
       `sudo pip3 install -U rospkg`     
* Install catkin_pkg
       `sudo pip3 install -U catkin_pkg`
* Install pymongo
       `sudo pip3 install pymongo`   
